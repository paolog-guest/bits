Title: DebConf16: Convocatoria de propuestas
Date: 2016-03-24 10:00
Tags: debconf, debconf16, cfp
Slug: debconf16-cfp
Author: Allison Randal
Translator: Adrià García-Alzórriz
Lang: es
Status: published

El equipo de Contenidos de la DebConf tiene el placer de anunciar la
Convocatoria de propuestas para la [conferencia DebConf16](https://debconf16.debconf.org/),
que se celebrará en **Ciudad del Cabo, Sudáfrica** del 2 al 9 de julio de 2016.

## Enviar un evento

Para poder enviar un evento, vaya a "_Submit a talk_" en [su página de perfil en el sitio web de la DebConf16](https://debconf16.debconf.org/accounts/login/)
y describa su propuesta. Tenga en cuenta que los eventos no están
limitados a presentaciones tradicionales o sesiones informales (BoFs),
 sino que también serán bienvenidas propuestas de tutoriales, instalaciones
artísticas, debates o cualquier otro formato o evento que crea que podría
beneficiar a la comunidad de Debian.

Por favor, incluya un título breve, adecuado a un horario compacto, y
una descripción que motive el evento. Debería usar el campo "_Notes_"
para proporcionarnos información como oradores adicionales, restricciones
horarias o cualquier requisito que debamos tener en cuenta para su evento.

Las sesiones ordinarias pueden ser de 20 o 45 minutos de
duración (incluyendo tiempo para preguntas). Otra clase de sesiones (como
los talleres) podrían tener duraciones distintas. Por favor, elija la que
más se ajuste a su evento y detalle cualquier requisito especial.

## Calendario

El primer lote de propuestas aceptadas se anunciará en **abril**. Si depende
de tener una propuesta aceptada para poder asistir a la conferencia,
por favor envíela tan pronto como sea posible para que la podamos tener
en cuenta en este primer período de evaluación.

Todas las propuestas deben enviarse antes del **domingo 1 de mayo de 2016**
para que se puedan evaluar para el horario oficial.

## Temas y áreas

Aunque invitamos a que las propuestas sean sobre cualquier asunto
relacionado con Debian o software libre, tenemos un amplio abanico de temas
que podrían animar a la gente a enviar propuestas, incluyendo:

- Empaquetamiento en Debian, normativas e infraestructura
- Seguridad, protección y _hacking_
- Administración de sistemas Debian, automatización y orquestación
- Contenedores y computación en la nube con Debian
- Casos de éxito con Debian
- Debian en un contexto social, ético, legal o político
- _Blends_ (mezclas Debian), subproyectos, derivadas y proyectos que usan Debian
- Debian integrado y sistemas a nivel físico

## Cobertura de vídeo

Ofrecer las sesiones grabadas en vídeo amplifica la DebConf y es
uno de sus [objetivos](<http://debconf.org/goals.shtml). A menos que
los ponentes lo rechacen, los eventos oficiales se retransmitirán en
directo por Internet para promover la participación en remoto. Las
grabaciones se publicarán más tarde bajo la [licencia DebConf license](http://meetings-archive.debian.net/pub/debian-meetings/LICENSE),
 así como también las diapositivas de las presentaciones y las
publicaciones siempre que estén disponibles.

## Contacto y agradecimiento a los patrocinadores

DebConf no sería posible sin el apoyo generoso de todos nuestros
patrocinadores, especialmente nuestro patrocinador _Platinum_ [HPE].
DebConf16 todavía acepta patrocinadores: si está interesado,
por favor, ¡[contacte con nosotros](https://debconf16.debconf.org/contribute)!

No dude en contactar con el Equipo de contenido para cualquier cuestión
que tenga sobre el evento o cuestiones relacionadas con los eventos de
la DebConf en general. Puede encontrarnos en [content@debconf.org](mailto:content@debconf.org).

[HPE]: http://www.hpe.com/engage/opensource

## Recordatorio de registro

El registro para la DebConf está abierto. Inicie sesión en la web de la
DebConf16 y regístrese desde [su página de perfil](https://debconf16.debconf.org/accounts/profile/).

Para solicitar reembolsos (patrocinios) de alimentación, alojamiento o viaje,
debe haberse registrado antes del domingo 10 de abril del 2016.

Después de esta fecha, se aceptarán igualmente registros en cualquier
modalidad (básica, profesional y corporativa). No obstante, no le podremos
garantizar alojamiento en el campus y no se aceptarán más peticiones de
patrocinio.

Incluso si no está seguro de si podrá asistir, le recomendamos que se
registre ahora. Siempre puede cancelar su registro antes de la fecha
límite.
Sugerimos a los asistentes empezar a organizar sus desplazamientos tan
pronto como sea posible, por supuesto.

¡Esperamos verle en Ciudad del Cabo!
