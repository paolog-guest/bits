Title: Le projet Debian est au côté de la fondation GNOME dans sa défense contre les chasseurs de brevets
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

En 2012, le projet Debian a publié sa [position sur les brevets logiciels],
spécifiant la menace exercée par les brevets sur le logiciel libre.

La fondation GNOME a annoncé récemment qu'elle luttait contre une poursuite
prétendant que Shotwell, un gestionnaire de photos personnelles libre et à code
source ouvert, violerait prétendument un brevet.

Le projet Debian est fermement aux côtés de la fondation GNOME dans son travail
pour montrer au monde que la communauté du logiciel libre se défendra avec
vigueur contre les abus du système de brevets.

Nous vous engageons à lire ce
[billet de blog sur la défense de GNOME contre ce chasseur de brevets], et
à envisager de faire un don au
[fond de défense de GNOME contre les chasseurs de brevets].

[position sur les brevets logiciels]: https://www.debian.org/legal/patent
[billet de blog sur la défense de GNOME contre ce chasseur de brevets]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[fond de défense de GNOME contre les chasseurs de brevets]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
