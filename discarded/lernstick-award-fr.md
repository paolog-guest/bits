Title: « Lernstick », un dérivé de Debian remporte le prix Suisse du logiciel libre pour l'éducation
Slug: lernstick-award
Date: 2015-11-10 10:00
Author: Laura Arjona Reina
Tags: Lernstick, debian, derivatives, awards
Lang: fr
Status: draft

Le dérivé de Debian [Lernstick](FIXME_SEE_NOTE) a remporté le prix Suisse du logiciel libre pour l'éducation.

Lernstick, appelé en français « Laclé » est un dérivé de Debian Live destiné aux écoles. Il est principalement utilisé en Suisse, en Autriche et en Allemagne.
Lernstick fournit un environnement d'apprentissage et de travail sûr et mobile qui s'installe en général sur un support amovible (clé ou disque USB, carte mémoire, etc.) et qui peut éventuellement être installé sur un disque dur interne.

La distribution Lernstick fournit des programmes éducatifs, des logiciels multimédia, une suite bureautique complète, des jeux, des programmes de retouche de photos numériques, des applications internet et un espace pour les données personnelles.

Il est développé par l'Université de sciences appliqués du Nord Ouest de la Suisse (Fachhochschule Nordwestschweiz ) et educa.ch. Il est en développement continuel depuis sa création. Sa dernière version, basée sur Debian 8 Jessie, a été publiée le 9 octobre 2015.

Les [Open Source Awards Suisses](http://www.ossawards.ch/) distinguent les sociétés, établissements publics, les communautés du logiciel libre ou les personnalités qui ont fait montre d'audace et d'innovation dans le développement ou l'adoption des logiciels à source libre. Lernstick a remporté, en 2015, le prix spécial du logiciel libre pour l'éducation, parrainé par IBM.


Liens pour en savoir plus :
* [La page Lernstick dans le recensement des dérivés de Debian](https://wiki.debian.org/Derivatives/Census/Lernstick)
* [Lernstick - Laclé ](https://lacle.educa.ch/fr/clé-de-lélève-0)
* [Lernstick - résumé en anglais](http://www.imedias.ch/projekte/lernstick/lernstick_abstract_english.cfm)


NOTE
Three webs for Lernstick
* From Derivatives census: http://www.imedias.ch/projekte/lernstick/
* The offical web page in German: https://lernstick.educa.ch/#
* The offical web page in french: https://lacle.educa.ch/fr/clé-de-lélève-0
